﻿using Microsoft.EntityFrameworkCore;
using RecolorIA.Web.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RecolorIA.Web.Data
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<ArtPiece> ArtPieces { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
    }
}
