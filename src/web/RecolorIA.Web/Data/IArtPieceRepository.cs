﻿using RecolorIA.Web.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RecolorIA.Web.Data
{
    public interface IArtPieceRepository
    {
        Task CreateAsync(ArtPiece piece);
        Task<IList<ArtPiece>> GetArtPieces();
        Task<ArtPiece> GetArtPieceById(Guid id);
    }
}