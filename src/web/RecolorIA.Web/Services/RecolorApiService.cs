﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace RecolorIA.Web.Services
{
    public class RecolorService
    {
        private readonly HttpClient _client;

        public RecolorService(HttpClient client)
        {
            client.BaseAddress = new Uri("http://recoloria.api");

            client.DefaultRequestHeaders.Add(
                "Accept",
                "text/plain");
            client.DefaultRequestHeaders.Add(
                "User-Agent", 
                "RecolorIA.Web");

            _client = client;
        }

        public async Task<string> GetMessageAsync()
        {
            var response = await _client.GetAsync("");
            
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStringAsync();
        }

        public async Task<Uri> SendImageUrlAsync(Uri imageUrl, int maxColors)
        {
            //var content = new StringContent(
            //    JsonSerializer.Serialize(imageUrl.ToString()),
            //    Encoding.UTF8,
            //    "text/plain");

            var parameters = new Dictionary<string, string>
            {
                ["imgURL"] = imageUrl.ToString(),
                ["nbCluster"] = maxColors.ToString()
            };

            using var response =
                await _client.PostAsync("fromUrl", new FormUrlEncodedContent(parameters));

            response.EnsureSuccessStatusCode();

            return await Task.FromResult(response.RequestMessage.RequestUri);
        }

        public async Task<Uri> SendImageFileAsync(string filename, byte[] content, int maxColors)
        {
            using var multipartContent = new MultipartFormDataContent
            {
                { new ByteArrayContent(content), "file", filename },
                { new StringContent(maxColors.ToString()), "nbCluster" }
            };

            using var response = await _client.PostAsync("fromFile", multipartContent);

            response.EnsureSuccessStatusCode();

            return await Task.FromResult(response.RequestMessage.RequestUri);
        }

    }
}
