from flask import Flask, flash, request, redirect, url_for, session, send_file
from urllib.parse import urlparse
from werkzeug.utils import secure_filename
from flask_session import Session
from sklearn.cluster import KMeans
import urllib.request
import numpy as np
import pandas as pd
import pickle5 as pickle
import os
import matplotlib.image as mread
import matplotlib.image as mpimg
import random
import string
import ssl
import random
import uuid
from rec import recommendation
ssl._create_default_https_context = ssl._create_unverified_context
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}

UPLOAD_FOLDER = './data/UPLOADED/'
PROCESSED_FOLDER = './data/PROCESSED'
MODEL ='./data/RECDATA/Content_base.pickle'
TABLE ='./data/RECDATA/test2.csv'

app = Flask(__name__)
sess = Session()
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['PROCESSED_FOLDER'] = PROCESSED_FOLDER
app.config['MODEL'] = MODEL
app.config['TABLE'] = TABLE
app.secret_key = 'super secret key'
app.config['SESSION_TYPE'] = 'filesystem'
sess.init_app(app)

# RECOMMENDATION
table = pd.read_csv(app.config['TABLE'])
model = pickle.load(open(app.config['MODEL'],'rb'))


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def applyKMEANS(img, nbCluster):
    kmeans = KMeans( n_clusters = nbCluster )
    data = img.reshape(img.shape[0]*img.shape[1],img.shape[2])
    data = np.array(data)

    # Find clusters
    kmeans.fit(data)
    found = kmeans.predict(data)
    found = found.reshape(img.shape[0], img.shape[1])
    newImage = np.zeros(img.shape)
    
    # Generates random colors
    colors = []
    for j in range(0,nbCluster):
    	color = []
    	for i in range(0,img.shape[2]):
    		color.append(random.uniform(0, 1))
    	colors.append(color)

    j = 0
    # Recolor image
    for i in kmeans.cluster_centers_ :
        newImage[found==j] = colors[j]
        j+=1

    return newImage

@app.route('/', methods=['GET'])
def home():
    return '''
    <!doctype html>
    <title>Recolor API Test Page</title>
    
    <h2> Recolor API on LocalFile </h2>
    <form method=post action=/fromFile enctype=multipart/form-data>
      Local File <input type=file name=file> <br>
      Number of colors to change <input type=number name=nbCluster value=6> <br>
      <input type=submit value=Upload>
    </form>
    <br>
    <h2> Recolor API on Image Url </h2>
    <form method=post action=/fromUrl enctype=x-www-form-urlencoded>
        Image Url <input type=text name=imgURL> <br>
        Number of colors to change <input type=number name=nbCluster value=6> <br>
        <input type=submit value=Upload>
    </form>
    <br>
    <h2> Recommendation API </h2>
    <form method=post action=/recommendation enctype=x-www-form-urlencoded>
        <input type=text name=imgid>
        <input type=submit value=Recommendation>
    </form>
    '''


@app.route('/results')
def get_image():
    return send_file(app.config['PROCESSED_FOLDER']+"/"+request.args.get('filename'), mimetype='image/gif')


@app.route('/fromUrl', methods=['POST'])
def pull_image():
    if request.form['imgURL']:
        url = request.form['imgURL']
        nbCluster = int(request.form['nbCluster'])
        local_filename, headers = urllib.request.urlretrieve(url)
        img = mread.imread(local_filename)
        filename = str(uuid.uuid4()).upper() + "-" + os.path.basename(urlparse(url).path)
        filename = secure_filename(filename)
        mpimg.imsave(app.config['UPLOAD_FOLDER']+"/"+filename, img)
        mpimg.imsave(app.config['PROCESSED_FOLDER']+"/"+filename, applyKMEANS(img,nbCluster))
        return redirect(url_for('get_image', filename=filename))
    return "PAS COMPRENDU DSL"

@app.route('/fromFile', methods=['POST'])
def upload_file():
    if request.method == 'POST':

        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if not file :
            return redirect(request.url)
        if file and allowed_file(file.filename):
            # UUID is being fed by the front-end now.
            #filename = str(uuid.uuid4()).upper() + "-" + os.path.basename(file.filename)
            filename = os.path.basename(file.filename)
            filename = secure_filename(filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            nbCluster = int(request.form['nbCluster'])
            img = mread.imread(app.config['UPLOAD_FOLDER']+'/'+filename)
            mpimg.imsave(app.config['PROCESSED_FOLDER']+"/"+filename, applyKMEANS(img, nbCluster))
            return redirect(url_for('get_image',
                                    filename=filename))
    return "PAS COMPRENDU DSL"


@app.route('/recommendation/', methods = ['POST'])

def reccomend_request():

    suggest_dict = recommendation(int(request.form['imgid']),table,model)
    return suggest_dict

if __name__ == "__main__":
    # Only for debugging while developing
    #app.run(host='0.0.0.0', debug=True, port=80)
    app.run()


